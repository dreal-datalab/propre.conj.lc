#' Converti une date en trimestre
#'
#' @param date une date
#'
#' @return une chaine de caractère
#' @export
#' @examples
#' date <- lubridate::ymd('20120301')
#' en_trimestre(date)
#' @importFrom dplyr case_when
#' @importFrom lubridate month year
en_trimestre <- function(date){
  trim <- dplyr::case_when(
    lubridate::month(date) <= 3 ~ "1er",
    lubridate::month(date) <= 6 ~ "2\u00e8me",
    lubridate::month(date) <= 9 ~ "3\u00e8me",
    lubridate::month(date) <= 12 ~ "4\u00e8me"
  )
  return(
    paste(trim,"trimestre", lubridate::year(date))
  )
}

#' Liste des textes nécessaires pour la publication pour un indicateur donné sur une zone donnée
#'
#' @param indicateur Nom de l'indicateur.
#' @param stat trimestriel ("trimestre") ou cumul annuel
#' @param zone nom de la zone
#'
#' @return une liste
#' @export
#'
#' @importFrom dplyr filter mutate case_when select
#' @importFrom lubridate years ymd
#' @importFrom purrr transpose
#' @importFrom stringr str_to_sentence
creer_chiffre_clef <- function(indicateur = NULL,
                               stat = "trimestre",
                               zone = "Pays de la Loire") {
  if (is.null(indicateur)) stop("La variable indicateur doit \u00eatre renseign\u00e9e")
  get_serie_indicateur_pour_publication(indicateur) %>%
    dplyr::filter(.data$statistique == stat,
                  .data$Zone == zone) %>%
    dplyr::filter(.data$periode== max(.data$periode)) %>%
    dplyr::mutate(valeur = ifelse(.data$statistique == "moyenne annuelle" & .data$sommable == TRUE,
                                  .data$valeur*4,
                                  .data$valeur),
                  valeur = format_fr_nb(.data$valeur, dec = 0),
                  evolution_sans_signe = format_fr_pct(.data$evolution, dec = 1,sign = FALSE),
                  evolution = format_fr_pct(.data$evolution, dec = 1),
                  periode_n_moins_1 = dplyr::case_when(.data$sommable~paste("au",en_trimestre(.data$periode-lubridate::years(1))),
                                                       !.data$sommable~paste("fin",format(lubridate::ymd(.data$periode)-lubridate::years(1),format = "%B %Y")
                                                       )
                  ),
                  periode = dplyr::case_when(.data$sommable~paste("au",en_trimestre(.data$periode)),
                                             !.data$sommable~paste("fin",format(lubridate::ymd(.data$periode),format = "%B %Y"))
                  ),
                  periode_avec_maj =stringr::str_to_sentence(.data$periode)
    ) %>%
    dplyr::select(.data$periode,.data$periode_n_moins_1,.data$periode_avec_maj,.data$valeur,.data$evolution,.data$evolution_sans_signe) %>%
    purrr::transpose() %>%
    .[[1]]

}


#' Créer le code markdown à insérer dans la publication pour les chiffres clefs
#'
#' @param indicateur le nom de l'indicateur (character)
#'
#' @importFrom drealdown my_icon
#' @importFrom glue glue
#' @export
creer_chiffre_clef_publication <- function(indicateur = NULL){
  res <-   glue::glue("<hr>
{drealdown::my_icon('la-truck', size=2)}<b>{creer_chiffre_clef(indicateur = indicateur)$valeur}</b> entreprises cr\u00e9\u00e9es {creer_chiffre_clef(indicateur = indicateur)$periode}<br>
{drealdown::my_icon('la-line-chart', size=2)}<b>{creer_chiffre_clef(indicateur = indicateur)$evolution}</b> sur un an<br>


<hr>")
  return(res)
}


#' Calculer les taux des chiffres clefs.
#'
#' @param indicateur_num numeric, le numerateur du taux indicateur
#' @param indicateur_denom numeric, le denominateur du taux indicateur
#' @param stat character, le type de stat, "trimestre" par defaut, sinon cumul annuel
#' @param zone character, le nom de la zone, "Pays de la Loire" par defaut
#'
#' @importFrom dplyr bind_rows filter select mutate lag case_when
#' @importFrom lubridate years ymd %m-%
#' @importFrom purrr transpose
#' @importFrom rlang sym
#' @importFrom stringr str_to_sentence
#' @importFrom tidyr pivot_wider
#' @export
creer_taux_clef <- function(indicateur_num = NULL,
                            indicateur_denom = NULL,
                            stat = "trimestre",
                            zone = "Pays de la Loire") {
  if (is.null(indicateur_num)) stop("La variable indicateur_num doit \u00eatre renseign\u00e9e")
  if (is.null(indicateur_denom)) stop("La variable indicateur_denom doit \u00eatre renseign\u00e9e")
  data1 <- get_serie_indicateur_pour_publication(indicateur_num)
  data2 <- get_serie_indicateur_pour_publication(indicateur_denom)

  res <- dplyr::bind_rows(data1, data2) %>%
    dplyr::filter(.data$statistique == stat,
                  .data$Zone == zone) %>%
    dplyr::filter(.data$periode== max(.data$periode) | .data$periode == max(.data$periode) %m-% lubridate::years(1)) %>%
    dplyr::select(.data$Zone,.data$periode,.data$indicateur,.data$valeur,.data$sommable) %>%
    tidyr::pivot_wider(names_from = "indicateur",values_from = "valeur") %>%
    dplyr::mutate(taux = 100*(!!rlang::sym(indicateur_num) / !!rlang::sym(indicateur_denom)),
                  evolution_taux = .data$taux-dplyr::lag(.data$taux)) %>%
    dplyr::filter(.data$periode== max(.data$periode)) %>%
    dplyr::mutate(                  taux = format_fr_pct(.data$taux, dec = 1,sign = FALSE),
                                    evolution_taux_sans_signe = format_fr_nb(.data$evolution_taux, dec = 1,sign = FALSE),
                                    evolution_taux = format_fr_nb(.data$evolution_taux, dec = 1,sign = TRUE),
                                    periode_n_moins_1 = dplyr::case_when(sommable~paste("au",en_trimestre(.data$periode-lubridate::years(1))),
                                                                         !sommable~paste("fin",format(lubridate::ymd(.data$periode)-lubridate::years(1),format = "%B %Y")
                                                                         )
                                    ),
                                    periode = dplyr::case_when(sommable~paste("au",en_trimestre(.data$periode)),
                                                               !sommable~paste("fin",format(lubridate::ymd(.data$periode),format = "%B %Y"))
                                    ),
                                    periode_avec_maj =stringr::str_to_sentence(.data$periode)
    ) %>%
    dplyr::select(.data$periode,.data$periode_n_moins_1,.data$periode_avec_maj,.data$taux,.data$evolution_taux,.data$evolution_taux_sans_signe) %>%
    purrr::transpose() %>%
    .[[1]]



  return(res)
}
