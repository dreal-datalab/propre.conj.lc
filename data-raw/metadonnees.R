## code to prepare `metadonnees_indicateurs` dataset goes here
library(dplyr)
library(forcats)
library(tidyr)
library(readxl)
library(stringr)
path <- file.path('inst','extdata','r_graphiques.xlsx')
metadonnees_indicateurs <-read_excel(path,na="ND") %>%
  replace_na(list(st="")) %>%
  tricky::set_standard_names() %>%
  mutate(libelle=str_c(t,"\n",tolower(st)),
         libelle=factor(libelle),
         libelle=fct_inorder(libelle),
         clef=as.logical(clef),
         cumul_annuel=as.logical(cumul_annuel)
  )


load('data/data_conjoncture.rda')
list <- ls(pattern = "data_")
liste_indicateurs <- purrr::map_dfr(list,~levels(pull(get(.x),indicateur)) %>%
             tibble::tibble() %>%
             purrr::set_names('indicateur') %>%
             mutate(dataframe = .x) %>%
             select(dataframe,indicateur))
usethis::use_data(metadonnees_indicateurs,liste_indicateurs, overwrite = TRUE,internal = TRUE)
